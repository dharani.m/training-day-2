import customException.MyCustomException;
import customException.MyOtherException;
import enums.Colors;
import enums.Warning_Enum;
import innerClass.OuterClass;
import netscape.javascript.JSObject;
import serialization.SerializableClass;
import serialization.SerializableClass2;
import threads.MyRunnable;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        //Inner Classes (static and non-static)
        OuterClass oc = new OuterClass("First Outer Class");
        oc.printObjectName();
        oc.incrementCount();
        oc.incrementCount();


        //Enum with constructor and method
        Colors color = Colors.RED;
        System.out.println(color);


        Warning_Enum warning = Warning_Enum.RED;
        warning.printWarningLeve();

        //File Operations using Reader, Writer (Character Stream)
        File file = new File(System.getProperty("user.dir")+"\\sample.txt");
        try{
            if(file.createNewFile()) {
                System.out.println("File Created!");
            }
            Writer fw = new FileWriter(file);
            fw.write("Start of the file content...\nNew line 1\nNew line 2");
            System.out.println("Written contents to file!");
            fw.close();

            Reader fr = new FileReader(file);
            int ch;
            StringBuilder fileContents = new StringBuilder();
            while((ch = fr.read()) != -1){
                fileContents.append((char)ch);
            }

            System.out.println("File Contents :\n<<\n" + fileContents + "\n>>");

            if(file.delete()){
                System.out.println("File Deleted!");
            }
        } catch (Exception e){
            System.out.println("Error : " + e.getMessage());
        }

        //File Operations using InputStream and OutputStream (Byte Stream)
        file = new File(System.getProperty("user.dir")+"\\sampleStreamFile.txt");
        try{
            if(file.createNewFile()) {
                System.out.println("File Created!");
            }

            FileOutputStream fos = new FileOutputStream(file);
            fos.write("Some text for file..\nFor file manipulation\nUsing Streams".getBytes(StandardCharsets.UTF_8));
            System.out.println("Written contents to file!");
            fos.close();

            FileInputStream fis = new FileInputStream(file);
            Scanner sc = new Scanner(file);
            sc.nextLine();
            int ch;
            StringBuilder fileContents = new StringBuilder();
            while((ch = fis.read()) != -1){
                fileContents.append((char)ch);
            }
            fis.close();
            System.out.println("File Contents :\n<<\n" + fileContents + "\n>>");

            if(file.delete()){
                System.out.println("File Deleted!");
            }
        } catch (Exception e){
            System.out.println("Error : " + e.getMessage());
        }

        //Serialization and Deserialization
        SerializableClass2 dependantObject = new SerializableClass2(25,"Serializable Object 2");
        SerializableClass serializableObject = new SerializableClass(95,"Serializable Object 1",dependantObject);
        System.out.println("Before Serialization..");
        serializableObject.printValues();
        File f = new File("serialFile.ser");
        try{
            //Serializing
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(serializableObject);
            fos.close();
            oos.close();
            System.out.println("\nSerialization Complete!\n");

            //Deserializing
            SerializableClass newSerialObject;
            FileInputStream fis = new FileInputStream(f);
            ObjectInputStream ois = new ObjectInputStream(fis);
            newSerialObject = (SerializableClass) ois.readObject();
            fis.close();
            ois.close();
            System.out.println("After DeSerialization..");
            newSerialObject.printValues();

        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        //Using HttpURLConnection class to get data from google.com
        try {
            URL url = new URL("https://www.google.com/");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("accept", "application/json");

            InputStream responseStream = connection.getInputStream();
            File fileGoogleResult = new File("resp.txt");
            writeFile(responseStream,fileGoogleResult);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        //Custom Exceptions
        int num = 24;
        try{
            if(num<25){
                throw new MyCustomException("Value "+ num +" less than 25");
            } else{
                throw new MyOtherException(num);
            }
        } catch (MyCustomException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch(MyOtherException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        try {
            int num2 = exceptionThrowingMethod(6);
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        System.out.println("After handling all errors...");

        try {
            URL jokesURL = new URL("https://official-joke-api.appspot.com/jokes/random");
            HttpURLConnection jokesConnection = (HttpURLConnection) jokesURL.openConnection();
            InputStream jokesStream = jokesConnection.getInputStream();
            byte[] s = jokesStream.readAllBytes();
            System.out.println(new String(s));
        } catch (Exception e){
            System.out.println(e.getMessage());
        }


        //Thread Programming
        MyRunnable runnableObj = new MyRunnable("sample1.txt", 20000);
        MyRunnable runnableObj2 = new MyRunnable("sample2.txt",100000);
        MyRunnable runnableObj3 = new MyRunnable("sample3.txt",7200);
        MyRunnable runnableObj4 = new MyRunnable("sample4.txt",40000);
        Thread thread = new Thread(runnableObj);
        Thread thread2 = new Thread(runnableObj2);
        Thread thread3 = new Thread(runnableObj3);
        Thread thread4 = new Thread(runnableObj4);
        thread.start();
        thread2.start();
        thread3.start();
        thread4.start();

        //JDBC
        Connection myConnection = null;
        Statement myStatement= null;
        ResultSet myResult = null;
        try {
            myConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/People","juser","juserpassword");
            myStatement = myConnection.createStatement();
            displayDBData(myStatement);
            System.out.println("\nDisplaying with Prepared statement:");
            displayStudentWithName(myConnection,"Abby");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void displayStudentWithName(Connection con,String name){
        try {
           PreparedStatement statement = con.prepareStatement("select * from student where name=?");
           statement.setString(1,name);
           ResultSet res = statement.executeQuery();
           displayResult(res);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void displayResult(ResultSet rs){
        try {
            while (rs.next()){
                System.out.println("id: "+rs.getString("id")+" Name: "+rs.getString("name")+" Email: "+rs.getString("email")+" Phone: "+rs.getString("phone"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void displayDBData(Statement s){
        try {
            ResultSet myResult = s.executeQuery("select * from student");
            while (myResult.next()){
                System.out.println("id: "+myResult.getString("id")+" Name: "+myResult.getString("name")+" Email: "+myResult.getString("email")+" Phone: "+myResult.getString("phone"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void writeFile(InputStream inputData,File file){
        try (FileOutputStream outputStream = new FileOutputStream(file, false)) {
            int read;
            byte[] bytes = new byte[8142];
            while ((read = inputData.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    public static int exceptionThrowingMethod(int x) throws MyCustomException {
        if(x==5){
            return 5;
        }else{
            throw new MyCustomException("Thrown from exceptionThrowingMethod, for the value passed-> "+x);
        }
    }
}
