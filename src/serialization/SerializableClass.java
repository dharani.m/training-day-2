package serialization;

import java.io.Serializable;

public class SerializableClass implements Serializable {
    private int a;
    private String str;
    private SerializableClass2 sc2;

    public SerializableClass(int a, String str, SerializableClass2 sc2){
        this.a = a;
        this.str = str;
        this.sc2 = sc2;
    }

    public void printValues(){
        System.out.println(this.a+ " " +this.str);
        this.sc2.printValues2();
    }
}



