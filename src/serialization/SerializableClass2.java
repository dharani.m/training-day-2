package serialization;

import java.io.Serializable;

public class SerializableClass2 implements Serializable {
    private String name2;
    private int a2;

    public SerializableClass2(int a, String name){
        this.a2 = a;
        this.name2 = name;
    }

    public void printValues2(){
        System.out.println("From Second Serializable Class : "+this.a2+" "+this.name2);
    }
}
