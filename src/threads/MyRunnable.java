package threads;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

public class MyRunnable implements Runnable{
    private String fileName;
    private int iterations;

    public MyRunnable(String fileName, int iterations){
        this.fileName = fileName;
        this.iterations = iterations;
    }
    @Override
    public void run() {
        File f = new File(fileName);
        FileOutputStream fos = null;

        try {
            byte[] bytes = "This is a sample text to be added to the files\n".getBytes(StandardCharsets.UTF_8);
            fos = new FileOutputStream(f);
            System.out.println(Thread.currentThread().getName() + " started....");
            for (int i = 0; i < this.iterations; i++) {
                fos.write(bytes[i % bytes.length]);
            }
            System.out.println(Thread.currentThread().getName() + " ended....");
        }catch (Exception e){
            e.printStackTrace();
        } finally {
           try{
               fos.close();
           }catch (Exception e){
               e.printStackTrace();
           }
        }
    }

}
