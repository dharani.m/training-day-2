package innerClass;

import java.util.Comparator;

public class OuterClass {
    private static String className;
    private static int count = 0;
    private int num;
    public OuterClass(String name){
        className = name;
        count++;
    }
    private static void displayName(){
        System.out.println(className);
    }

    public void incrementCount(){
        InnerClass ic = new InnerClass();
        ic.incrementCount();
        System.out.println("Count incremented using inner class method.\nCount = "+ count);
    }

    public void printObjectName(){
        InnerClass ic = new InnerClass();
        ic.displayName();

    }
    public class InnerClass{
        public String name = "Inner Class";
        public void displayCount(){
            System.out.println("From Inner Class : \nObjects count = " + count + num);
        }

        public void incrementCount(){
            count++;
        }
        public void displayName(){
            OuterClass.displayName();
        }
    }
}
