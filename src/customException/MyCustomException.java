package customException;

public class MyCustomException extends Exception{
    private String message;
    public MyCustomException(String e){
        this.message = e;
    }
    public String getMessage(){
        return this.message;
    }
}
