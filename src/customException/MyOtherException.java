package customException;

public class MyOtherException extends Exception{
    private int num;
    public MyOtherException(int num){
        this.num = num;
    }
    public String getMessage(){
        return "The given number " + this.num +" resulted in an Exception from MyOtherException";
    }
}
