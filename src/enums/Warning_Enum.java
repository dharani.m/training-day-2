package enums;

public enum Warning_Enum {
    RED (4),
    ORANGE (3),
    YELLOW(2),
    GREEN(1);

    private int warningLevel;

    Warning_Enum(int level){
        warningLevel = level;
    }

    public void printWarningLeve(){
        System.out.println(this.warningLevel);
    }
}
